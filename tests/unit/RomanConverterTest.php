<?php

use KataRomanNumerals\RomanConverter;

class RomanConverterTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider numeral_roman_conversion_provider
     */
    public function test_returns_roman_when_numeral_given($numeral, $expectedRoman)
    {
        $converter = new RomanConverter();

        $roman = $converter->fromNumeral($numeral);

        $this->assertEquals($expectedRoman, $roman);
    }

    public function numeral_roman_conversion_provider()
    {
        return [
            [1, 'I'],
            [5, 'V'],
            [2, 'II'],
            [4, 'IV'],
            [8, 'VIII'],
            [369, 'CCCLXIX'],
            [448, 'CDXLVIII'],
            [309, 'CCCIX'],
        ];
    }
}
