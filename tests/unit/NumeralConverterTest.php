<?php

namespace KataRomanNumerals;

use PHPUnit\Framework\TestCase;

class NumeralConverterTest extends TestCase
{
    /**
     * @dataProvider roman_numeral_conversion_provider
     */
    public function test_returns_numeral_when_roman_given($roman, $expectedNumber)
    {
        $converter = new NumeralConverter();

        $number = $converter->fromRoman($roman);

        $this->assertEquals($expectedNumber, $number);
    }

    public function roman_numeral_conversion_provider()
    {
        return [
            ['I', 1],
            ['V', 5],
            ['II', 2],
            ['IV', 4],
            ['VIII', 8],
            ['CCCLXIX', 369],
            ['CDXLVIII', 448],
            ['CCCIX', 309],
        ];
    }
}
