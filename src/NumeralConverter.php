<?php

declare(strict_types=1);

namespace KataRomanNumerals;

class NumeralConverter
{
    const ROMAN_TO_NUMBER = [
        'M' => 1000,
        'D' => 500,
        'C' => 100,
        'L' => 50,
        'X' => 10,
        'V' => 5,
        'I' => 1,
    ];

    public function fromRoman(string $romanNumber): int
    {
        $result = 0;
        $previousNumber = null;
        $buffer = 0;

        foreach (\str_split($romanNumber) as $single) {
            $converted = $this->convertSingleRoman($single);

            if (isset($previousNumber) && $previousNumber < $converted) {
                $result -= $buffer;
                $buffer = 0;
            } elseif (isset($previousNumber) && $previousNumber > $converted) {
                $result += $buffer;
                $buffer = 0;
            }

            $buffer += $converted;

            $previousNumber = $converted;
        }

        return $result + $buffer;
    }

    private function convertSingleRoman($romanNumber): int
    {
        return self::ROMAN_TO_NUMBER[$romanNumber];
    }
}