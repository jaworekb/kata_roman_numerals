<?php

namespace KataRomanNumerals;

class RomanConverter
{
    public function fromNumeral(int $number): string
    {
        $result = '';

        $simpleNumber = (int) \floor($number / 1000);
        $number = $number % 1000;
        $result .= $this->simpleRoman($simpleNumber, 'M', null, null);

        $simpleNumber = (int) \floor($number / 100);
        $number = $number % 100;
        $result .= $this->simpleRoman($simpleNumber, 'C', 'D', 'M');

        $simpleNumber = (int) \floor($number / 10);
        $number = $number % 10;
        $result .= $this->simpleRoman($simpleNumber, 'X', 'L', 'C');

        $result .= $this->simpleRoman($number, 'I', 'V', 'X');

        return $result;
    }

    private function simpleRoman(int $simpleNumber, string $one, ?string $five, ?string $ten)
    {
        switch ($simpleNumber) {
            case 0:
                return '';
                break;
            case 1:
            case 2:
            case 3:
                return $this->duplicate($simpleNumber, $one);
                break;
            case 4:
                return $one . $five;
                break;
            case 5:
                return $five;
                break;
            case 6:
            case 7:
            case 8:
                return $five . $this->duplicate($simpleNumber - 5, $one);
                break;
            case 9:
                return $one . $ten;
            default:
                throw new \InvalidArgumentException('Number must be lower thant 10');
        }
    }

    private function duplicate(int $simpleNumber, string $char): string
    {
        $result = '';
        while ($simpleNumber > 0) {
            $result .= $char;
            $simpleNumber--;
        }

        return $result;
    }
}